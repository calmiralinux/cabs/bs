# Calmira GNU/Linux-libre Automated Building System

Автоматизированная система сборки дистрибутива Calmira GNU/Linux-libre.

## Usage

### Building

- **Dependencies:** `cargo` - Rust building system

```bash
cargo build --release
```

### Installation

```bash
strip -s target/release/bs
sudo cp -v target/release/bs /usr/bin
```

### Syntax

```bash
bs -m MODE
```

`MODE` - режим работы системы сборки. Режимы работы описываются в одноимённом
файле с расширением `.toml`. Пример:

```bash
# Создание файла с описанием режима работы
vim calmira_base_system.toml # Пользователь вводит какие-то данные...
# Выполнение сборочных инструкций
bs -m calmira_base_system
```

## Build instructions

Сборочные инструкции представляют собой BASH-скрипт, которому передаются
следующие переменные окружения:

1. `PATH=/bin:/sbin:/usr/bin:/usr/sbin`
2. `CALM=/mnt/calm`
3. `CALM_TGT=x86_64-calm-linux-gnu`
4. `MAKEFLAGS='-j4'`
5. `SRC_DIR=/mnt/calm/usr/src` или `SRC_DIR=/usr/src`

Пример сборочных инструкций:

```bash
#!/bin/bash -e

NAME=имя_пакета
VERSION=версия_пакета

cd $SRC_DIR
tar -xf $NAME-$VERSION.tar.xz # Распаковка архива с исходным кодом
cd $NAME-$VERSION # Переход в распакованную директорию с исходниками

./configure \
  --prefix=/usr \
  --bindir=/bin \
  --sbindir=/sbin \
  --disable-static

make
make DESTDIR=$DST install

cd $SRC_DIR
clean_pkgs # Инструкция для очистки директории с исходным кодом от лишних файлов
```

## Configuration example

Базовая конфигурация (`./bs.toml`):

```toml
build_modes = [
  "prepare",
  "toolchain1",
  "toolchain2",
  "core",
  "extended"
]
```

Для каждого режима имя файла = имя режима + `.toml`:

```toml
[settings]
path = "/bin:/sbin:/usr/bin:/usr/sbin"
calm = "/mnt/calm"
calm_tgt = "???"
makeflags = "-j4"
src_dir = "/mnt/calm/usr/src"

[build]
packages = [
  "acl",
  "attr",
  "glibc",
  ...
]
```

## Ключи использования

- `--mode`, `-m` - выбор конфигурации сборки;

## Документация

### Пользовательская

Перейдите в директорию `docs/` и выполните:

```bash
mdbook serve --open
```

> **Внимание!**
>
> Для этого вам необходим mdBook:

```bash
cargo install mdbook
```

### Для разработчиков

```bash
cargo doc --no-deps --open
```
