/*!
  Модуль для исполнения сборочных инструкций программного обеспечения
*/

use std::collections::HashMap;
use std::process::Command;
use std::time::SystemTime;
use std::{fs, io};

use anyhow::{anyhow, Result};
use serde::Serialize;
use toml;

use crate::config;

/// Содержит информацию о выполнении сборочных инструкций
#[derive(Serialize, Debug)]
pub struct BuildInstructions {
    /// Список собранных пакетов
    pub builds: Vec<BuildStatus>,
}

/// Информация о статусе сборки пакетов
#[derive(Serialize, Debug)]
pub struct BuildStatus {
    /// Имя инструкции (скрипта), который выполнялся
    pub instruction: String,
    /// Время его сборки в секундах
    pub build_time: u64,
    /// Статус сборки (код, который вернул скрипт) - 0-255
    pub build_status: usize,
}

fn _get_val(value: &Option<String>) -> String {
    match value {
        Some(x) => x.to_string(),
        None => " ".to_string(),
    }
}

/// Исполняет указанный `script` скрипт.
///
/// ## Коды завершения
///
/// Скрипт является успешно завершённым, если он вернул код, равный нулю (0). В
/// том случае, если вернулся код, отличный от 0, метод спросит у пользователя
/// продолжение. Варианты ответа:
///
/// - `c` - продолжение сборки *следующих* пакетов;
/// - `r` - пересборка этого пакета;
/// - `a/q/e` - завершение сборки;
///
/// В том случае, если пользователь ответил `c`, сборка "ошибочного" пакета
/// прервётся, продолжится сборка всего остального ПО. Если пользователь ответил
/// `r`, то сборка "ошибочного" пакета повторится. Если пользователь увидел
/// сообщение о продолжении или окончании сборки, он может отредактировать
/// сборочные инструкции так, как требуется, после чего возобновить сборку
/// пакета с ошибкой.
fn run_script(script: &str, conf: &config::SettingsConf) -> i32 {
    let calm = _get_val(&conf.calm);
    let calm_tgt = _get_val(&conf.calm_tgt);
    let makeflags = _get_val(&conf.makeflags);
    let ninjajobs = _get_val(&conf.ninjajobs);

    let mut code = -1;
    let mut env = HashMap::new();

    let check = match conf.check.unwrap_or(false) {
        true => "yes",
        false => "no",
    };

    env.insert("PATH".to_owned(), conf.path.clone());
    env.insert("CALM".to_owned(), calm);
    env.insert("CALM_TGT".to_owned(), calm_tgt);
    env.insert("SRC_DIR".to_owned(), conf.src_dir.clone());
    env.insert("MAKEFLAGS".to_owned(), makeflags.clone());
    env.insert("TESTSUITEFLAGS".to_owned(), makeflags);
    env.insert("NINJAJOBS".to_owned(), ninjajobs);
    env.insert("CHECK".to_owned(), check.to_owned());

    while code < 0 {
        code = match run(script, &env) {
            Ok(c) => c,
            Err(why) => dialog(&why.to_string(), script),
        };
    }

    code
}

// Непосредственно запускает скрипт в отдельном потоке
fn run(script: &str, env: &HashMap<String, String>) -> Result<i32> {
    // run the script and get a handle to the running child process
    let run = Command::new(script).envs(env).status()?;

    if run.success() {
        let code = run.code().unwrap_or(0);
        Ok(code)
    } else {
        Err(anyhow!("Build package '{script}' error: {}", run.code().unwrap_or(-1)))
    }
}

fn dialog(err_description: &str, script: &str) -> i32 {
    eprintln!("The script '{script}' exit with an error: {}", err_description);
    eprintln!("Continue building (c), rebuild this pkg (r) or abort (a/e/q)?",);

    let mut guess = String::new();

    if io::stdin().read_line(&mut guess).is_err() {
        eprintln!("Failed to read line");
        return 666;
    }

    guess = guess.trim().to_string();

    match guess.as_str() {
        // english and russian keymap
        "c" | "с" => {
            eprintln!("Build package '{script}' complete!");
            0
        }
        "r" => {
            eprintln!("Rebuild package '{script}'...");
            -1
        }
        "a" | "e" | "q" => {
            eprintln!("Aborted!");
            666
        }
        _ => {
            eprintln!("Unknown input: {guess}");
            666
        }
    }
}

/// Последовательно выполняет сборочные инструкции
pub fn build_instructions(conf: config::Configuration) -> BuildInstructions {
    let packages = &conf.build.packages;
    let build_conf = &conf.settings;

    let mut builds = Vec::new();

    for package in packages {
        println!("\n\x0b===> Build package {}...", &package);
        let pkg_path = String::from(&build_conf.dir) + &"/".to_owned() + &package;

        let time = SystemTime::now();

        let code = run_script(&pkg_path, &build_conf);

        let elapsed_time = match time.elapsed() {
            Ok(elapsed) => elapsed.as_secs(),
            Err(_) => 0,
        };

        let data = BuildStatus {
            instruction: pkg_path.to_string(),
            build_time: elapsed_time,
            build_status: code as usize,
        };

        println!("Build time (package '{package}'): {elapsed_time} secs.");

        if code == 666 {
            break;
        } else {
            builds.push(data);
        }
    }

    let build_data = BuildInstructions { builds };
    let build_data_str: String = toml::to_string(&build_data).unwrap();
    let build_stage_status = "/tmp".to_string() + "/bs-build-" +
        &build_conf.dir + ".toml";

    match fs::write(build_stage_status, build_data_str) {
        Ok(_) => println!("Write build information OK"),
        Err(why) => eprintln!("Error writing build info: {why}"),
    }

    build_data
}
