use clap::Parser;

#[derive(Parser)]
#[command(author = "Michail Krasnov <michail383krasnov@mail.ru>")]
#[command(version = "0.1")]
struct Args {
    #[arg(short, long)]
    mode: String,

    #[arg(short, long, default_value_t = false)]
    check: bool,
}

fn main() {
    let args = Args::parse();
    let mode = &args.mode;

    if args.check {
        if !bs::check_packages(&mode) {
            eprintln!("Packages checking error! (mode: {})", &mode);
            std::process::exit(1);
        } else {
            std::process::exit(0);
        }
    }

    let mode = args.mode + ".toml";
    bs::build_packages(&mode);
}
