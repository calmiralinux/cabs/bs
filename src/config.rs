/*!
  Метод для парсинга конфигурационных файлов системы сборки.

  ## Функции:

  1. Сериализация конфигурационных файлов:
        - Позволяет писать свои параметры в конфиги.
  2. Десериализация конфигурационных файлов:
        - Читает параметры из конфигурационных файлов.

  Структура каждого конфига системы сборки строго стандартизирована и не может
  быть изменена извне программы.

  ## Базовый конфигурационный файл

  ```ignore
  [settings]
  dir = "core"
  path = "/bin:/sbin:/usr/bin:/usr/sbin"
  calm = "/mnt/calm"
  calm_tgt = "x86_lfs_linux_gnu"
  makeflags = "-j4"
  src_dir = "/mnt/calm/usr/src"

  [build]
  packages = [
    "binutils-p1",
    "gcc-p1",
    "linux-api-headers",
    "glibc"
  ]
  ```

  > Для каждого режима сборки указывается свой конфигурационный файл.
*/

use std::{fs, io::{self, Error, ErrorKind}, path::PathBuf};
use toml;
use serde::{Serialize, Deserialize};

/// Описание базового конфигурационного файла
///
/// Структура, описывающая конфигурацию определённого режима сборки.
/// Все сценарии сборки в CABS разделены на режимы: к примеру,
/// сборка кросс-компилятора - это один режим, сборка тулчейна -
/// второй, сборка редакций дистрибутива - третий, а сборка
/// установочных пакетов - четвёртый.
///
/// Имя конфигурационного файла = имя режима + `.toml`
#[derive(Debug, Serialize, Deserialize)]
pub struct Configuration {
    /// Параметры системы сборки, которые передаются позже
    /// сборочным инструкциям в качестве переменных окружения
    pub settings: SettingsConf,
    /// Содержит единственный параметр: список пакетов,
    /// которые требуется собрать. Пакеты сдесь
    /// указываются в том порядке, в котором их нужно собрать
    pub build:    BuildConf,
}

/// Параметры системы сборки, которые передаются сборочным
/// инструкциям как переменные окружения
#[derive(Debug, Serialize, Deserialize)]
pub struct SettingsConf {
    /// Параметр определяет имя директории со сборочными инструкциями
    pub dir: String,
    /// Параметр, переопределяющий переменную окружения
    /// `PATH`. Для каждого режима сборки требуется
    /// указание своей переменной `PATH`. Она предназначена для
    /// того, чтобы оболочка операционной системы GNU/Linux
    /// могла определять, в каких директориях следует искать
    /// бинарные двоичные файлы программ, а затем запускать их.
    /// Из-за этого пользователю не требуется вводить что-то
    /// вроде `/usr/bin/gcc main.c -o main -larchive ...`, т.е.
    /// не требуется указания полного пути до бинарника.
    ///
    /// > Директории в этом параметре, как и в переменной `PATH`,
    /// > разделяются **двоеточиями** (`:`):
    ///
    /// ```ignore
    /// /bin:/usr/bin:/usr/local/bin
    /// ```
    pub path:      String,
    /// Параметр указывает директорию, в которой находится
    /// собираемая система. Значение по умолчанию - `/mnt/calm`.
    ///
    /// По умолчанию, в эту директорию устанавливаются все
    /// программы из временного инструментария (тулчейна) -
    /// режимы `toolchain1` и `toolchain2`.
    pub calm:      Option<String>,
    /// TODO
    pub calm_tgt:  Option<String>,
    /// Параметр указывает директорию, в которой находится
    /// исходный код необходимых пакетов. Исходный код должен
    /// находиться уже в **распакованном** состоянии.
    pub src_dir:   String,
    /// Указывает флаги для системы сборки GNU Make. К примеру,
    /// сборка пакетов в 4 потока:
    ///
    /// ```ignore
    /// -j4
    /// ```
    pub makeflags: Option<String>,
    /// Указывает число потоков для системы сборки `meson`
    pub ninjajobs: Option<String>,
    /// Параметр указывает на прохождение тестирования
    /// корректности сборки программного обеспечения
    pub check: Option<bool>,
}

/// Содержит список пакетов для сборки
#[derive(Debug, Serialize, Deserialize)]
pub struct BuildConf {
    /// Список пакетов в том порядке, в котором их требуется собрать
    pub packages: Vec<String>,
}

/// Типаж для файла конфигурации
pub trait ConfigProvider {
  /// Читает конфигурационный файл и возвращает содержимое как строку
  fn read(&self) -> io::Result<String>;
}

/// Полный путь файла конфигурации
pub struct ConfigPath(pub PathBuf);

impl ConfigProvider for ConfigPath {
    fn read(&self) -> io::Result<String> {
        fs::read_to_string(&self.0)
    }
}

impl Configuration {
    /// Читает указанный конфигурационный файл (`path`) и выполняет его
    /// десериализацию.
    ///
    /// ## Errors
    ///
    /// Возвращает ошибки в случае, если указанный конфиг не найден, либо его
    /// структура не соответствует `Configuration`, либо данные в файле не в
    /// кодировке UTF-8.
    ///
    /// ## Exapmles
    ///
    /// ```ignore
    /// use std::path::PathBuf;
    /// use bs::config::{Configuration, ConfigPath};
    ///
    /// fn main() {
    ///     let conf = Configuration::parse_conf(&ConfigPath(PathBuf::from("/etc/bs/core.toml"))).unwrap();
    ///     println!("{:#?}", &conf.build.packages);
    /// }
    /// ```
    pub fn parse_conf(data_provider: &dyn ConfigProvider) -> io::Result<Self> {
        data_provider.read().map(|cfg| {
            toml::from_str(&cfg)
                .map_err(|_| Error::new(ErrorKind::Other, "Can not parse configuration"))
        })?
    }
}

#[cfg(test)]
mod test {
  use super::*;

  struct TestConf(&'static str);

  impl ConfigProvider for TestConf {
    fn read(&self) -> io::Result<String> {
      Ok(self.0.to_string())
    }
  }

  #[test]
  fn test_config_ok() {
    let cfg = r#"
    [settings]
    dir = "core"
    path = "/bin:/sbin:/usr/bin:/usr/sbin"
    calm = "/mnt/calm"
    calm_tgt = "x86_lfs_linux_gnu"
    makeflags = "-j4"
    src_dir = "/mnt/calm/usr/src"
    check = true

    [build]
    packages = [
      "binutils-p1",
      "gcc-p1",
      "linux-api-headers",
      "glibc"
    ]
    "#;

    let result = Configuration::parse_conf(&TestConf(cfg));
    assert!(result.is_ok(), "Configuratuion wasn't correct.");
  }

  #[test]
  fn test_config_incorrect() {
    let cfg = r#"
    [settings]
    dir = "core"
    path = "/bin:/sbin:/usr/bin:/usr/sbin"
    calm = "/mnt/calm"
    calm_tgt = "x86_lfs_linux_gnu"
    makeflags = "-j4"
    src_dir = "/mnt/calm/usr/src"
    check = check
    "#;

    let result = Configuration::parse_conf(&TestConf(cfg));
    assert!(result.is_err());
  }
}
