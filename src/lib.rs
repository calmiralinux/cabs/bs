pub mod config;
pub mod build;

use std::path::{Path, PathBuf};
use std::process::exit;

use crate::config::{Configuration, ConfigPath};

pub fn build_packages(edition: &str) -> u64 {
    let config_file = Path::new(&edition);
    let conf_str = config_file.to_str().unwrap_or("???");

    let config_data = match Configuration::parse_conf(&ConfigPath(PathBuf::from(config_file))) {
        Ok(x) => x,
        Err(why) => {
            eprintln!("Config {} parsing error: {}",
                      conf_str, why);
            exit(1);
        },
    };

    build::build_instructions(config_data);

    0
}

pub fn check_packages(edition: &str) -> bool {
    let conf = String::from(edition) + ".toml";

    let conf_file = Path::new(&conf);
    let conf_str = conf_file.to_str().unwrap_or("???");

    let conf_data = match Configuration::parse_conf(&ConfigPath(PathBuf::from(conf_file))) {
        Ok(x) => x,
        Err(why) => {
            eprintln!("Config {} parsing error: {}",
                      conf_str, why);
            exit(1);
        },
    };

    let files = conf_data.build.packages;

    println!("==> Check packages (work mode: {})", &edition);

    let mut ret_code = true;
    for file in files {
        let _file = Path::new(&edition).join(&file);

        if !_file.exists() || !_file.is_file() {
            eprintln!("    --> [ERROR] {}", &file);
            ret_code = false;
        } else {
            println!("    --> [OK] {}", &file);
        }
    }

    ret_code
}
